Input
=====

Tiny golang lib to handle keyboard and mouse input so it can be easily consumed by games.


    i := New()

    // forward event input, first loop
    i.SendKey(KeyA, Press)
    i.SendKey(KeyB, Press)
    i.Tick()

    // forward event input, second loop
    i.SendKey(KeyB, Release)
    i.SendKey(KeyC, Press)
    i.Tick()

    // handle inputs
    i.Pressed(KeyA)      // true
    i.JustPressed(KeyA)  // false
    i.Age(KeyA)          // 1
    i.JustPressed(KeyC)  // true
    i.JustReleased(KeyB) // true
