package input

import (
	"fmt"
	"testing"
)

func TestDoc(t *testing.T) {
	i := New()

	// forward event input
	i.SendKey(KeyA, Press)
	i.SendKey(KeyB, Press)
	i.Tick()

	i.SendKey(KeyB, Release)
	i.SendKey(KeyC, Press)
	i.Tick()

	// handle inputs
	fmt.Println(i.Pressed(KeyA))      // true
	fmt.Println(i.JustPressed(KeyA))  // false
	fmt.Println(i.Age(KeyA))          // 1
	fmt.Println(i.JustPressed(KeyC))  // true
	fmt.Println(i.JustReleased(KeyB)) // true
}
