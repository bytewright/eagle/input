package input

import "github.com/go-gl/glfw/v3.3/glfw"

func SendGLFWKey(i Out, key glfw.Key, action glfw.Action) {
	if action == glfw.Release {
		i.SendKey(Key(key), Release)
	} else if action == glfw.Press {
		i.SendKey(Key(key), Press)
	}
}

func SendGLFWMouse(i Out, key glfw.MouseButton, action glfw.Action) {
	if action == glfw.Release {
		i.SendKey(Key(key+mouseIndex), Release)
	} else {
		i.SendKey(Key(key+mouseIndex), Press)
	}
}
