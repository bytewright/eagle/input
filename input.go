package input

import (
	"gitlab.com/bytewright/gmath/math32"
)

const (
	pressedMask = 1 << 30
	countMask   = (1 << 26) - 1
)

type keyState uint32

type Action int

const (
	Press   Action = 1
	Release Action = 2
)

type In interface {
	JustPressed(Key) bool
	JustReleased(Key) bool
	// Pressed returns true if all of the provided keys are in pressed state
	Pressed(...Key) bool
	Mouse() math32.Vec2
	Scrolled() math32.Vec2
	Age(key Key) int
	Typed() []rune
	JustPressedKeys() []Key
	JustReleasedKeys() []Key
}

type Out interface {
	Tick()
	SendKey(k Key, a Action)
	SendChar(char rune)
	SetMousePosition(p math32.Vec2)
	SendScrolled(p math32.Vec2)
}

type Input interface {
	In
	Out
}

// input provides the state of all keys and mouse buttons
type input struct {
	states        [int(AllButtonsLast + 1)]keyState
	collected     []event
	justPressed   []Key
	justReleased  []Key
	mousePosition math32.Vec2
	scrolled      math32.Vec2
	chars         []rune
}

type event struct {
	key    Key
	action Action
}

func New() Input {
	states := [int(AllButtonsLast + 1)]keyState{}
	for i := 0; i < len(states); i++ {
		states[i] = keyState(1)
	}
	return &input{
		states: states,
		chars:  []rune{},
	}
}

// out interface

func (i *input) SendKey(key Key, a Action) {
	i.collected = append(i.collected, event{key: Key(key), action: a})
}

func (i *input) Tick() {
	i.justPressed = i.justPressed[0:0]
	i.justReleased = i.justReleased[0:0]

	// increase count by one
	for j := range i.states {
		c := (i.states[j] + 1) & countMask
		i.states[j] = i.states[j]&(pressedMask) | c
	}

	for _, e := range i.collected {
		if e.action == Press {
			i.states[e.key] = pressedMask
			i.justPressed = append(i.justPressed, e.key)
		}
		// TODO do keep release events when state is pressed and age is 0
		// so a press/release during one cycle is still recorded
		if e.action == Release {
			i.states[e.key] = 0
			i.justReleased = append(i.justReleased, e.key)
		}
	}
	i.collected = i.collected[0:0]
	i.chars = i.chars[0:0]
	i.scrolled = math32.Vec2{}
}

func (i *input) SetMousePosition(p math32.Vec2) {
	i.mousePosition = p
}

func (i *input) SendChar(char rune) {
	i.chars = append(i.chars, char)
}

func (i *input) SendScrolled(s math32.Vec2) {
	i.scrolled = i.scrolled.Add(s)
}

// in interface

func (i *input) JustPressed(key Key) bool {
	return i.Pressed(key) && i.Age(key) == 0
}

func (i *input) JustPressedKeys() []Key {
	return i.justPressed
}

func (i *input) JustReleased(key Key) bool {
	return !i.Pressed(key) && i.Age(key) == 0
}

func (i *input) JustReleasedKeys() []Key {
	return i.justReleased
}

func (i *input) Mouse() math32.Vec2 {
	return i.mousePosition
}

func (i *input) Scrolled() math32.Vec2 {
	return i.scrolled
}

// Pressed returns true if all of the provided keys are in pressed state
func (i *input) Pressed(keys ...Key) bool {
	for _, k := range keys {
		if i.states[k]&pressedMask == 0 {
			return false
		}
	}
	return true
}

// Age returns the number of frames the key is in it's state (pressed or release)
// if 0 it means that the key got pressed or released in the current frame
// if 10 it means that the key got pressed or released 10 frames ago
func (i *input) Age(key Key) int {
	return int(i.states[key] & countMask)
}

func (i *input) Typed() []rune {
	return append([]rune{}, i.chars...)
}
