package input

import (
	"testing"

	"gitlab.com/akabio/expect"
)

func TestInputKeyPress(t *testing.T) {
	i := New()
	i.SendKey(KeyA, Press)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
	expect.Value(t, "a", i.Age(KeyA)).ToBe(0)
	i.Tick()
	expect.Value(t, "a", i.Age(KeyA)).ToBe(1)
	i.Tick()
	expect.Value(t, "a", i.Age(KeyA)).ToBe(2)
}

// func TestInputKeyRelease(t *testing.T) {
// 	i := NewInput()
// 	i.SendKey(glfw.KeyA, glfw.Press)
// 	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
// 	i.SendKey(glfw.KeyA, glfw.Release)
// 	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
// 	expect.Value(t, "a", i.Age(KeyA)).ToBe(0)
// 	i.Tick()
// 	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(false)
// 	expect.Value(t, "a", i.Age(KeyA)).ToBe(0)
// 	i.Tick()
// 	expect.Value(t, "a", i.Age(KeyA)).ToBe(1)
// }

func TestInputPressReleaseLater(t *testing.T) {
	i := New()
	i.SendKey(KeyA, Press)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
	i.SendKey(KeyA, Release)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(false)
	expect.Value(t, "a", i.Age(KeyA)).ToBe(0)
}

// func TestInputReleaseAndPressInSameTick(t *testing.T) {
// 	i := New()
// 	i.SendKey(glfw.KeyA, glfw.Press)
// 	i.Tick()
// 	i.SendKey(glfw.KeyA, glfw.Release)
// 	i.SendKey(glfw.KeyA, glfw.Press)
// 	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
// 	i.Tick()
// 	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
// 	expect.Value(t, "a", i.Age(KeyA)).ToBe(1)
// }

func TestInputTwoKeys(t *testing.T) {
	i := New()
	i.SendKey(KeyA, Press)
	i.SendKey(KeySpace, Press)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
	expect.Value(t, "space", i.Pressed(KeySpace)).ToBe(true)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
	expect.Value(t, "space", i.Pressed(KeySpace)).ToBe(true)
	i.SendKey(KeySpace, Release)
	i.Tick()
	expect.Value(t, "a", i.Pressed(KeyA)).ToBe(true)
	expect.Value(t, "space", i.Pressed(KeySpace)).ToBe(false)
}
